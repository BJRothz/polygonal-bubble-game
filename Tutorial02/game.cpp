#include "game.h"


game::game()
{
	noOfBubbles = 50;
	bubbleCount = 5;

	bubbles = new polygons[50];

}

game::~game()
{

	delete[] bubbles;
	bubbles = NULL;
}

int game::getNoOfBubbles(void)
{
	return noOfBubbles;
}

int game::getScore(void)
{
	return score;
}

int game::getHighscore(void)
{
	return highscore;
}

int game::getBubbleCount(void)
{
	return bubbleCount;
}

void game::setNoOfBubbles(int x)
{
	noOfBubbles = x;
}

void game::setScore(int x)
{
	score = x;
}

void game::setHighscore(int x)
{
	highscore = x;
}

void game::setBubbleCount(int x)
{
	bubbleCount = x;
}

void game::updateHighscore(void)
{
	if (score > highscore)
	{
		highscore = score;
	}
}

void game::shoot(bool doFire)
{
	if (player.getReloaded() == 1 && doFire == 1)
	{
		bubbles[bubbleCount - 1].setXOffset(player.getXOffset());
		bubbles[bubbleCount - 1].setYOffset(player.getYOffset());
		bubbles[bubbleCount - 1].setRotation(player.getRotation());
		bubbles[bubbleCount - 1].setSpeed(0.008f);

		player.setReloaded(0);
	}
}

void game::reload(void)
{
	int randType = rand() % 3;

	bubbleCount++;

	bubbles[bubbleCount - 1].setXOffset(.0f);
	bubbles[bubbleCount - 1].setYOffset(-0.6f);
	bubbles[bubbleCount - 1].setPolgonType(randType);
	bubbles[bubbleCount - 1].setPolygonVertices();
	bubbles[bubbleCount - 1].setSpeed(0.0f);

	player.setReloaded(1);
	player.setFire(0);
}