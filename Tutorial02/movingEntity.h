#pragma once
#include "entity.h"
#include "windows.h"

using namespace std;

class movingEntity : public entity
{
public:

	movingEntity();

	//get function prototypes
	float getSpeed(void);
	bool getDirection(void);
	float getRotation(void);

	//set function prototypes
	void setSpeed(float thisSpeed);
	void setDirection(bool cond); 
	void setRotation(float rot);

	//functions
	void move(void);

protected:
	float speed;
	bool direction;
	float rotation;


};