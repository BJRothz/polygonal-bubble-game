#pragma once
#include "polygons.h"
#include "playerArrow.h"
#include "windows.h"
#include <iostream>
#include <fstream>

class game
{
public:
	playerArrow player;
	polygons* bubbles = new polygons;
	fstream highscoreFile;

	game();

	~game();

	//get prototypes
	int getNoOfBubbles(void);
	int getScore(void);
	int getHighscore(void);
	int getBubbleCount(void);

	//set prototyes
	void setNoOfBubbles(int);
	void setScore(int);
	void setHighscore(int);
	void setBubbleCount(int);

	//other functions
	void updateHighscore(void);
	void shoot(bool doFire);
	void reload(void);

protected:
	int noOfBubbles;	//total number of bubbles
	int score;
	int highscore;
	int bubbleCount;	//current bubble count on screen

};