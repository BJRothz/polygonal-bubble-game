#include "playerArrow.h"

playerArrow::playerArrow()
{
	fire = 0;
	reloaded = 1;
	xOffset = 0.0f;
	yOffset = 0.0f;
	rotation = 0.0f;

	bullet = new polygons;

	speed = 0.0f;
}

playerArrow::~playerArrow()
{
	delete[] bullet;
	bullet = NULL;
}

bool playerArrow::getFire(void)
{
	return fire;
}

bool playerArrow::getReloaded(void)
{
	return reloaded;
}

void playerArrow::setFire(bool cond)
{
	fire = cond;
}

void playerArrow::setReloaded(bool cond)
{
	reloaded = cond;
}