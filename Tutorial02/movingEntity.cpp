#include "movingEntity.h"


movingEntity::movingEntity()
{
	speed = 0.0f;
	rotation = 0.0f;
	direction = 1;
}


float movingEntity::getSpeed(void)
{
	return speed;
}

bool movingEntity::getDirection(void)
{
	return direction;
}

float movingEntity::getRotation(void)
{
	return rotation;
}

void movingEntity::setSpeed(float thisSpeed)
{
	speed = thisSpeed;
}

void movingEntity::setDirection(bool cond)
{
	direction = cond;
}

void movingEntity::setRotation(float rot)
{
	rotation = rot;
}

void movingEntity::move(void)
{
	if (direction)
	{
		speed = 0.05f;
	}
	else
	{
		speed = -0.05f;
	}
}