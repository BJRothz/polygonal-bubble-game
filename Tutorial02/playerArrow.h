#pragma once
#include "polygons.h"

class playerArrow : public movingEntity
{
public:
	playerArrow();

	~playerArrow();

	polygons* bullet = new polygons;

	//get functions for variables
	bool getFire(void);
	bool getReloaded(void);

	//set functions for variables
	void setFire(bool cond);
	void setReloaded(bool cond);

	//other functions
	void reload(void);

protected:
	bool fire;
	bool reloaded;
};