#include "entity.h"


entity::entity()
{
	xOffset = 1.0f;
	yOffset = 1.0f;

}

float entity::getXOffset(void)
{
	return xOffset;
}

float entity::getYOffset(void)
{
	return yOffset;
}

void entity::setXOffset(float x)
{
	xOffset = x;
}

void entity::setYOffset(float y)
{
	yOffset = y;
}

