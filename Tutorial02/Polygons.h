#pragma once
#include "entity.h"
#include "movingEntity.h"

class polygons : public movingEntity
{
public:

	polygons();

	~polygons();

	//get prototypes
	short getPolygonType(void);
	short getPolygonVertices(void);
	bool getIsMoving(void);

	//set prototypes
	void setPolgonType(short type);
	void setPolygonVertices(void);
	void setIsMoving(bool cond);

	//functions
	void isOnEdge(void);
	void changeDirection(void);

protected:
	short polygonType;
	short polygonVertices;
	bool isMoving;
	bool alive;
};