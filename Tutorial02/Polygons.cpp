#include "Polygons.h"

polygons::polygons()
{
	xOffset = 1.0f;
	yOffset = 1.0f;
	rotation = 0.0f;

	direction = 1;
}

polygons::~polygons()
{

}

short polygons::getPolygonType(void)
{
	return polygonType;
}

short polygons::getPolygonVertices(void)
{
	return polygonVertices;
}

void polygons::setPolgonType(short type)
{
	polygonType = type;
}

void polygons::setPolygonVertices(void)
{
	switch (polygonType)
	{
	case 0:
		polygonVertices = 3;
		break;

	case 1:
		polygonVertices = 4;
		break;

	case 2:
		polygonVertices = 5;
		break;
	}
}
