//--------------------------------------------------------------------------------------
// File: Tutorial02.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

cbuffer cbPerObject
{
	float4x4 WVP;
	float4 color;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR;
};

float4 VS(float4 inPos : POSITION) : SV_POSITION
{
	float4 Pos = mul(inPos, WVP);

	return Pos;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(float4 color : COLOR) : SV_Target
{
	return color;   
}


//--------------------------------------------------------------------------------------
